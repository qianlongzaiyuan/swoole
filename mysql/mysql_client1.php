<?php
/************************************************************
 * Copyright (C), 1993, Dacelve. Tech., Ltd.
 * FileName : mysql_client.php
 * Author   : Lizhijian
 * Version  : 1.0
 * Date     : 2018/1/17 10:13
 * Description   :
 * Function List :
 * History  :
 * <author>    <time>    <version >    <desc>
 * Lizhijian   2018/1/17   1.0          init
 ***********************************************************/
$sql = 'SELECT nickname,avatar FROM fa_admin WHERE id = 1';

////使用连接池~耗时6S
$client = new swoole_client(SWOOLE_SOCK_TCP);
$client->connect('120.79.209.186', 9508, 10) or die("连接失败");
$client->send($sql);

$data = $client->recv();//阻塞接受返回的结果
$back = array();

var_dump(json_decode($data, true));
$client->close();


